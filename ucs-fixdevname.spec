Name: ucs-fixdevname
Version: 1
Release: 1
Summary: Fix device names on some hardware
Requires: udev
Group: System/Kernel
Packager: UCS Platforms <unix-support@ucs.cam.ac.uk>
License: GPLv2
Source: ucs-fixdevname-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch

%Description
Some systems have network devices whose name changes each time they are
installed.  This is unhelpful, so this package detects some such systems
known to exist in the University of Cambridge Computing Service and
fixes their network device names.

This package supports the following systems:

Sun Fire X4100 M2

%Prep
%setup

%Install
mkdir -p "${RPM_BUILD_ROOT}/lib/udev/rules.d"
cp 71-ucs-fixdevname.rules "${RPM_BUILD_ROOT}/lib/udev/rules.d/."

%Files
%defattr(-,root,root)
/lib/udev/rules.d/71-ucs-fixdevname.rules
