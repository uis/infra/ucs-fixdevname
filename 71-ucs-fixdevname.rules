# Ensure deterministic network device naming for certain hardware.
# This file currently only supports the Sun Fire X4100 M2 but it
# should be easily extensible to other hardware.

# The general approach is to detect the precise model of hardware that
# we're running on and then match specific PCI devices and assign them
# fixed names.  The names are of the form "em*", where "*" is the number
# marked on the network port.

# In an ideal world, biosdevname would work for this, but some systems
# don't implement the SMBIOS type 41 table that biosdevname needs in
# order to work.

SUBSYSTEM!="net", GOTO="end"
KERNEL!="eth*",   GOTO="end"
ACTION!="add",    GOTO="end"
NAME=="?*",       GOTO="end"

ATTR{[dmi/id]sys_vendor}=="Sun Microsystems", \
  ATTR{[dmi/id]product_name}=="Sun Fire X4100 M2", GOTO="sunfirex4100m2"
ATTR{[dmi/id]sys_vendor}=="Sun Microsystems", \
  ATTR{[dmi/id]product_name}=="SUN FIRE X4150", GOTO="sunfirex4150"

GOTO="end"

# using NAME= instead of setting INTERFACE_NAME, so that persistent
# names aren't generated for these devices, they are "named" on each boot.

# Sun Fire X4100 M2 -- devices labelled "NET 0" to "NET 3"
LABEL="sunfirex4100m2"
KERNELS=="0000:00:0a.0", NAME="em0"
KERNELS=="0000:80:0a.0", NAME="em1"
KERNELS=="0000:86:01.0", NAME="em2"
KERNELS=="0000:86:01.1", NAME="em3"
GOTO="end"

# Sun Fire X4150 -- devices labelled "NET 0" to "NET 3"
LABEL="sunfirex4150"
KERNELS=="0000:04:00.0", NAME="em0"
KERNELS=="0000:04:00.1", NAME="em1"
KERNELS=="0000:0b:00.0", NAME="em2"
KERNELS=="0000:0b:00.1", NAME="em3"
GOTO="end"

LABEL="end"
